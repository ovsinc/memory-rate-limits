module gitlab.com/ovsinc/memory-rate-limits

go 1.15

require (
	github.com/StackExchange/wmi v0.0.0-20210224194228-fe8f1750fd46 // indirect
	github.com/go-ole/go-ole v1.2.5 // indirect
	github.com/gofiber/fiber/v2 v2.8.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/shirou/gopsutil/v3 v3.21.3
	github.com/stretchr/testify v1.6.1
	github.com/valyala/fasthttp v1.23.0
	go.uber.org/ratelimit v0.2.0
	golang.org/x/sys v0.0.0-20210426230700-d19ff857e887 // indirect
	google.golang.org/grpc v1.37.0
)
