module grpc-test-server

go 1.15

require (
	github.com/golang/protobuf v1.4.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.2
	github.com/sirupsen/logrus v1.4.2
	gitlab.com/ovsinc/memory-rate-limits v1.1.0
	google.golang.org/grpc v1.34.0
	google.golang.org/protobuf v1.25.0
)
