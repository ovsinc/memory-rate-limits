package grpc

import (
	"context"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/ovsinc/memory-rate-limits/rate"
)

type Limiter interface {
	Limit() rate.Limited
	UnarySkip(handler grpc.UnaryHandler) bool
	StreamSkip(handler grpc.StreamHandler) bool
}

func New() Limiter {
	conf := rate.RateLimitConfig{
		MemoryUsageBarrierPercentage: rate.DefaultRateLimitConfig.MemoryUsageBarrierPercentage,
		RequestsLimitPerSecond:       rate.DefaultRateLimitConfig.RequestsLimitPerSecond,
		BypassOnError:                rate.DefaultRateLimitConfig.BypassOnError,
		RateOnBypass:                 rate.DefaultRateLimitConfig.RateOnBypass,
	}

	return NewWithConfig(conf)
}

func NewWithConfig(config rate.RateLimitConfig) Limiter {
	if config.Skipper == nil {
		config.Skipper = rate.DefaultRateLimitConfig.Skipper
	}

	if config.Logger == nil {
		config.Logger = rate.DefaultRateLimitConfig.Logger
	}

	rl := rate.New(config)

	return &limiter{rate: rl, config: config}
}

type limiter struct {
	rate   rate.Ilimiter
	config rate.RateLimitConfig
}

func (rl *limiter) Limit() rate.Limited {
	defer func(begin time.Time) {
		rl.config.Logger.Debugf("memory-rate-limits execute with %s", time.Since(begin).String())
	}(time.Now())
	r, _ := rl.rate.Limit()
	return r
}

func (rl *limiter) UnarySkip(handler grpc.UnaryHandler) bool {
	return rl.config.Skipper(handler)
}

func (rl *limiter) StreamSkip(handler grpc.StreamHandler) bool {
	return rl.config.Skipper(handler)
}

//

// https://github.com/grpc-ecosystem/go-grpc-middleware/blob/master/ratelimit/ratelimit.go

// UnaryServerInterceptor returns a new unary server interceptors that performs request rate limiting.
func UnaryServerInterceptor(limiter Limiter) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		if limiter.UnarySkip(handler) {
			return handler(ctx, req)
		}

		if limiter.Limit() == rate.Fails {
			return nil, status.Errorf(codes.ResourceExhausted, "%s is rejected by memory-rate-limits middleware, please retry later.", info.FullMethod)
		}
		return handler(ctx, req)
	}
}

// StreamServerInterceptor returns a new stream server interceptor that performs rate limiting on the request.
func StreamServerInterceptor(limiter Limiter) grpc.StreamServerInterceptor {
	return func(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
		if limiter.StreamSkip(handler) {
			return handler(srv, stream)
		}
		if limiter.Limit() == rate.Fails {
			return status.Errorf(codes.ResourceExhausted, "%s is rejected by memory-rate-limits middleware, please retry later.", info.FullMethod)
		}
		return handler(srv, stream)
	}
}
