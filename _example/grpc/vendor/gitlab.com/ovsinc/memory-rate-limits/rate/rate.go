package rate

import (
	"time"

	"gitlab.com/ovsinc/memory-rate-limits/utils/memory"

	"go.uber.org/ratelimit"
)

// RateLimitConfig rate limit conf
type RateLimitConfig struct {
	// Skipper skip function
	Skipper Skipper

	// MemoryUsageBarrierPercentage the memory usage barrier after which the rate limit is enabled
	MemoryUsageBarrierPercentage float64

	// RequestsLimitPerSecond requests rate limit
	RequestsLimitPerSecond int

	// BypassOnError flag to use the bypass when there is an error getting information about the resources
	BypassOnError bool

	// RateOnBypass flag to use the rate limit when there is an error getting information about the resources
	RateOnBypass bool

	// Logger logger interface realization
	Logger Logger
}

// DefaultRateLimitConfig defailt config
var DefaultRateLimitConfig = RateLimitConfig{
	Skipper:                      NotSkip,
	MemoryUsageBarrierPercentage: 80,
	RequestsLimitPerSecond:       10,
	BypassOnError:                true,
	RateOnBypass:                 true,
	Logger:                       NewNoLog(),
}

// Limited return code type of Limitter
type Limited uint32

func (l Limited) String() string {
	var ret string
	switch l {
	case Unlimited:
		ret = "Unlimited"

	case RateLimited:
		ret = "RateLimited"

	case FailsBypassLimited:
		ret = "FailsBypassLimited"

	case Fails:
		ret = "Fails"

	case FailsBypass:
		ret = "FailsBypass"

	case UNDEFlimit:
		ret = "UNDEFlimit"
	}

	return ret
}

const (
	UNDEFlimit Limited = iota

	// Unlimited unlimit requests
	Unlimited

	// RateLimited rate limiting is enabled
	RateLimited

	// FailsBypass fails get information about the resources, but bypass is enabled
	FailsBypass

	// FailsBypassLimited fails get information about the resources, but rate limit is enabled
	FailsBypassLimited

	// Fails fails get information about the resources, but do not know what to do
	Fails
)

//

func percentage(cur, total uint64) float64 {
	if total < 1 {
		return 0
	}
	return float64(cur*100) / float64(total)
}

//

// Ilimiter limiter interface
type Ilimiter interface {
	Limit() (Limited, time.Time)
}

// New Limiter constructor
func New(config RateLimitConfig) Ilimiter {
	return &rateLimiter{
		Limiter: ratelimit.New(config.RequestsLimitPerSecond),
		config:  config,
		memo:    memory.New(),
		zero:    time.Unix(0, 0),
	}
}

type rateLimiter struct {
	memo memory.IMem
	ratelimit.Limiter
	config RateLimitConfig
	zero   time.Time
}

func (rl *rateLimiter) bypass() Limited {
	if rl.config.BypassOnError {
		if rl.config.RateOnBypass {
			return FailsBypassLimited
		}
		return FailsBypass
	}
	return Fails
}

func (rl *rateLimiter) limitMem() Limited {
	if rl.config.MemoryUsageBarrierPercentage < 1 {
		rl.config.Logger.
			Debugf("MemoryUsageBarrierPercentage < 1, skip")
		return Unlimited
	}

	usedMem, memUserErr := rl.memo.MemUsed()
	totalMem, memTotalerr := rl.memo.MemTotal()

	rl.config.Logger.
		Debugf("usedMem: %v of %v totalMem", usedMem, totalMem)

	if memUserErr != nil || memTotalerr != nil || totalMem < 1 {
		rl.config.Logger.
			Errorf("calculate the memory limit fails, bypass on error mode - %t", rl.config.BypassOnError)
		rl.config.Logger.
			Debugf("memUserErr = %v, memTotalerr = %v, totalMem = %v", memUserErr, memTotalerr, totalMem)
		return rl.bypass()
	}

	percentMem := percentage(usedMem, totalMem)

	rl.config.Logger.
		Debugf("percentMem = %v", percentMem)

	if percentMem < rl.config.MemoryUsageBarrierPercentage {
		return Unlimited
	}

	return RateLimited
}

func (rl *rateLimiter) Limit() (Limited, time.Time) {
	retMem := rl.limitMem()

	rl.config.Logger.
		Debugf("RAM: %s", retMem.String())

	switch {
	case retMem == RateLimited:
		rl.config.Logger.
			Warnf("rate limiting, RAM full")
		return RateLimited, rl.Take()

	case retMem == FailsBypassLimited:
		rl.config.Logger.
			Warnf("rate limiting, RAM check fails, bypass")
		return FailsBypassLimited, rl.Take()

	case retMem == FailsBypass:
		rl.config.Logger.
			Warnf("RAM check fails, bypass")
		return FailsBypass, rl.zero

	case retMem == Fails:
		rl.config.Logger.
			Warnf("RAM check fails, rize error")
		return Fails, rl.zero
	}

	rl.config.Logger.
		Debugf("RAM check OK")
	return Unlimited, rl.zero
}
