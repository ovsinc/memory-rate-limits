package utils

import (
	"errors"
	"io"
	"io/ioutil"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const (
	selfCGroupPath = "/proc/self/cgroup"
	readBytesLen   = 10 // int64 size + '0x0a' + '0x00'
)

// ErrNotOpen read from not opened file
var ErrNotOpen = errors.New("file not open")

var validInt = regexp.MustCompile("[-0-9]+")

// refer to https://github.com/containerd/cgroups/blob/318312a373405e5e91134d8063d04d59768a1bff/utils.go#L251
func ParseUint(s string, base, bitSize int) (uint64, error) {
	v, err := strconv.ParseUint(s, base, bitSize)
	if err != nil {
		intValue, intErr := strconv.ParseInt(s, base, bitSize)
		// 1. Handle negative values greater than MinInt64 (and)
		// 2. Handle negative values lesser than MinInt64
		if intErr == nil && intValue < 0 {
			return 0, nil
		} else if intErr != nil &&
			// intErr.(*strconv.NumError).Err == strconv.ErrRange &&
			errors.Is(intErr, strconv.ErrRange) &&
			intValue < 0 {
			return 0, nil
		}
		return 0, err
	}
	return v, nil
}

// ReadUint read and parse uint64 from file, read only 9 bytes
func ReadUint(path string) (uint64, error) {
	f, err := os.Open(path)
	if err != nil {
		return 0, err
	}
	defer f.Close()

	v := make([]byte, readBytesLen)

	_, err = f.Read(v)
	if !(err == nil || errors.Is(err, io.EOF)) {
		return 0, err
	}

	return ParseUint(string(validInt.Find(v)), 10, 64)

	// return parseUint(strings.Trim(string(v), "\n "), 10, 64)
}

// InContainer check is in conteiner
func InContainer() bool {
	v, err := ioutil.ReadFile(selfCGroupPath)
	if err != nil {
		return false
	}
	if strings.Contains(string(v), "docker") ||
		strings.Contains(string(v), "kubepods") ||
		strings.Contains(string(v), "containerd") {
		return true
	}
	return false
}

func NewParse(path string) (*Parse, error) {
	p := &Parse{}
	return p, p.Open(path)
}

// Parse parser for known files
type Parse struct {
	f *os.File
}

// Open open file for read only
func (p *Parse) Open(path string) (err error) {
	p.f.Close()
	p.f, err = os.Open(path)
	return err
}

// Close close opened file
// do not forget close it when file is unnided
func (p *Parse) Close() {
	p.f.Close()
}

// ReadUint read and parse uit64
func (p *Parse) ReadUint() (uint64, error) {
	if p.f == nil {
		return 0, ErrNotOpen
	}

	data := make([]byte, readBytesLen)

	_, _ = p.f.Seek(0, 0)

	_, err := p.f.Read(data)
	if !(err != nil || errors.Is(err, io.EOF)) {
		return 0, err
	}

	return ParseUint(strings.TrimSpace(string(data)), 10, 64)
}
