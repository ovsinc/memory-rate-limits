package main

//go:generate protoc --go_out=plugins=grpc:. test.proto

import (
	"context"
	"net"

	"google.golang.org/grpc"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_logrus "github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	grpc_ctxtags "github.com/grpc-ecosystem/go-grpc-middleware/tags"
	"github.com/sirupsen/logrus"

	mymiddle "gitlab.com/ovsinc/memory-rate-limits/middlewares/grpc"
)

/*
type TestServiceServer interface {
	Len(context.Context, *Empty) (*LenResponse, error)
	Add(context.Context, *Empty) (*Empty, error)
	Del(context.Context, *Empty) (*Empty, error)
	Burn(context.Context, *Empty) (*Empty, error)
}
*/

type svc struct {
	memoInst *memo
}

func (s *svc) Len(_ context.Context, _ *Empty) (*LenResponse, error) {
	return &LenResponse{Len: int32(s.memoInst.Len())}, nil
}

func (s *svc) Add(_ context.Context, _ *Empty) (*Empty, error) {
	s.memoInst.Add()
	return new(Empty), nil
}

func (s *svc) Del(_ context.Context, _ *Empty) (*Empty, error) {
	s.memoInst.Del()
	return new(Empty), nil
}

func (s *svc) Burn(_ context.Context, _ *Empty) (*Empty, error) {
	s.memoInst.Burn(2)
	return new(Empty), nil
}

func main() {
	logger := logrus.New()

	opts := []grpc_logrus.Option{
		grpc_logrus.WithLevels(grpc_logrus.DefaultCodeToLevel),
	}

	srvOpts := []grpc.ServerOption{
		grpc_middleware.WithStreamServerChain(
			grpc_ctxtags.StreamServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
			grpc_logrus.StreamServerInterceptor(logrus.NewEntry(logger), opts...),
			mymiddle.StreamServerInterceptor(mymiddle.New()),
			grpc_recovery.StreamServerInterceptor(),
		),
		grpc_middleware.WithUnaryServerChain(
			grpc_ctxtags.UnaryServerInterceptor(grpc_ctxtags.WithFieldExtractor(grpc_ctxtags.CodeGenRequestFieldExtractor)),
			grpc_logrus.UnaryServerInterceptor(logrus.NewEntry(logger), opts...),
			mymiddle.UnaryServerInterceptor(mymiddle.New()),
			grpc_recovery.UnaryServerInterceptor(),
		),
	}

	svc := &svc{memoInst: newMemo()}

	server := grpc.NewServer(srvOpts...)

	RegisterTestServiceServer(server, svc)

	lis, err := net.Listen("tcp", ":9000")
	if err != nil {
		panic("failed to listen: " + err.Error())
	}

	server.Serve(lis)
}
