// Based on PingCAP code
// see https://github.com/pingcap/tidb/blob/084e7190b8124f4b53ce83064547042a77ce3ae4/util/memory/meminfo.go

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// See the License for the specific language governing permissions and
// limitations under the License.

package memory

import (
	"sync/atomic"

	"github.com/shirou/gopsutil/v3/mem"
	"gitlab.com/ovsinc/memory-rate-limits/utils"
)

const (
	cGroupMemLimitPath = "/sys/fs/cgroup/memory/memory.limit_in_bytes"
	cGroupMemUsagePath = "/sys/fs/cgroup/memory/memory.usage_in_bytes"
)

//

// IMem memory amound interface
type IMem interface {
	// MemTotal returns the total amount of RAM on this system
	MemTotal() (uint64, error)

	// MemUsed returns the total used amount of RAM on this system
	MemUsed() (uint64, error)
}

// New ...
func New() IMem {
	if utils.InContainer() {
		return &MemCGroup{}
	}
	return &MemNormal{}
}

//

type MemNormal struct {
}

// MemTotal returns the total amount of RAM on this system in non-container environment.
func (mn *MemNormal) MemTotal() (uint64, error) {
	v, err := mem.VirtualMemory()
	return v.Total, err
}

// MemUsed returns the total used amount of RAM on this system in non-container environment.
func (mn *MemNormal) MemUsed() (uint64, error) {
	v, err := mem.VirtualMemory()
	return v.Used, err
}

//

type MemCGroup struct {
	memLimitCache  uint64
	memLimitCached uint32
}

// MemTotal returns the total amount of RAM on this system in container environment.
func (mcg *MemCGroup) MemTotal() (uint64, error) {
	cacheFlag := atomic.LoadUint32(&mcg.memLimitCached)
	if cacheFlag > 0 {
		return mcg.memLimitCache, nil
	}

	var err error

	mcg.memLimitCache, err = utils.ReadUint(cGroupMemLimitPath)
	if err != nil {
		return 0, err
	}

	atomic.StoreUint32(&mcg.memLimitCached, 1)

	return mcg.memLimitCache, nil
}

// MemUsed returns the total used amount of RAM on this system in container environment.
func (mcg *MemCGroup) MemUsed() (uint64, error) {
	return utils.ReadUint(cGroupMemUsagePath)
}
