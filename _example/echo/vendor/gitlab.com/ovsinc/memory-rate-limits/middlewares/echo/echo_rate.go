package echo

import (
	"net/http"

	echo "github.com/labstack/echo/v4"

	rate "gitlab.com/ovsinc/memory-rate-limits/rate"
)

// echo Middleware

// RateLimitMiddleware echo middleware with default conf
func RateLimitMiddleware(logger rate.Logger) echo.MiddlewareFunc {
	conf := rate.RateLimitConfig{
		Skipper:                      rate.NotSkip,
		MemoryUsageBarrierPercentage: rate.DefaultRateLimitConfig.MemoryUsageBarrierPercentage,
		RequestsLimitPerSecond:       rate.DefaultRateLimitConfig.RequestsLimitPerSecond,
		BypassOnError:                rate.DefaultRateLimitConfig.BypassOnError,
		RateOnBypass:                 rate.DefaultRateLimitConfig.RateOnBypass,
		Logger:                       logger,
	}
	return RateLimitWithConfig(conf)
}

// RateLimitWithConfig echo middleware with custom conf
func RateLimitWithConfig(config rate.RateLimitConfig) echo.MiddlewareFunc {
	if config.Skipper == nil {
		config.Skipper = rate.DefaultRateLimitConfig.Skipper
	}

	rl := rate.New(config)

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			if config.Skipper(c) {
				return next(c)
			}

			r, _ := rl.Limit()
			switch r {
			case rate.Fails:
				return c.NoContent(http.StatusTooManyRequests)

			default:
				return next(c)
			}
		}
	}
}
