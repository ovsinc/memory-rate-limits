package rate

// Logger logger interface with minimal set of methods
type Logger interface {
	Errorf(format string, msg ...interface{})
	Warnf(format string, msg ...interface{})
	Debugf(format string, msg ...interface{})
}

//

// NewNoLog Logger with no logging constructor
func NewNoLog() Logger {
	return &noLog{}
}

type noLog struct{}

func (l *noLog) Errorf(format string, msg ...interface{}) {}
func (l *noLog) Warnf(format string, msg ...interface{})  {}
func (l *noLog) Debugf(format string, msg ...interface{}) {}
