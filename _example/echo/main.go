package main

import (
	"context"
	"net/http"
	"strconv"

	"time"

	echo "github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	mymiddle "gitlab.com/ovsinc/memory-rate-limits/middlewares/echo"
)

var (
	memoInst *memo
)

func init() {
	memoInst = newMemo()
}

//

func Len(c echo.Context) error {
	return c.String(http.StatusOK, strconv.Itoa(memoInst.Len()))
}

func Add(c echo.Context) error {
	memoInst.Add()
	return c.NoContent(http.StatusOK)
}

func Del(c echo.Context) error {
	memoInst.Del()
	return c.NoContent(http.StatusOK)
}

func Burn(c echo.Context) error {
	memoInst.Burn(2)
	return c.NoContent(http.StatusOK)
}

//

func clean(ctx context.Context) {
	ticker := time.NewTicker(2 * time.Second)

	for {
		select {
		case <-ctx.Done():
			break
		case <-ticker.C:
			memoInst.Del()
		}
	}
}

//

func main() {
	e := echo.New()

	e.Debug = true

	e.Pre(middleware.RemoveTrailingSlash())
	e.Use(middleware.Logger())
	e.Use(mymiddle.RateLimitMiddleware(e.Logger))

	e.GET("/len", Len)
	e.GET("/add", Add)
	e.GET("/del", Del)
	e.GET("/burn", Burn)

	// ctx, cancelFn := context.WithCancel(context.TODO())
	// defer cancelFn()
	// go clean(ctx)

	e.Logger.Fatal(e.Start(":8000"))
}
