package main

import (
	"context"
	"crypto/rand"
	"runtime"
	"sync"
	"time"
)

const (
	randSliceSize = 1 * 1024 // 1KiB
	randCount     = 1024
)

// 100% CPU loads with time
func cpuBurn(ctx context.Context, burn <-chan time.Duration) {
	for {
		select {
		case <-ctx.Done():
			return
		case interval := <-burn:
			t := time.Tick(interval)
			go func() {
				for {
					select {
					case <-t:
						return
					case <-ctx.Done():
						return
					default:
					}

					for i := 0; i < 2147483647; i++ {
					}
					runtime.Gosched()
				}
			}()
		}
	}
}

func newMemo() *memo {
	m := &memo{
		mu:   new(sync.Mutex),
		data: make([]byte, 0),
		rnd:  make([]byte, randSliceSize),
		burn: make(chan time.Duration),
	}
	i, err := rand.Read(m.rnd)
	if err != nil || i != randSliceSize {
		panic("can`t read random")
	}

	m.ctx, m.cancelFn = context.WithCancel(context.TODO())

	go cpuBurn(m.ctx, m.burn)

	return m
}

type memo struct {
	data     []byte
	mu       *sync.Mutex
	rnd      []byte
	burn     chan time.Duration
	ctx      context.Context
	cancelFn context.CancelFunc
}

func (m *memo) Add() {
	m.mu.Lock()
	defer m.mu.Unlock()
	m.data = append(m.data, m.rnd...)
}

func (m *memo) Len() int {
	m.mu.Lock()
	defer m.mu.Unlock()
	return len(m.data)
}

func (m *memo) Del() {
	m.mu.Lock()
	defer m.mu.Unlock()
	if len(m.data) > 0 {
		m.data = m.data[:len(m.data)-len(m.rnd)]
	}
}

func (m *memo) Stop() {
	m.cancelFn()
}

func (m *memo) Burn(intervalSec int64) {
	m.burn <- time.Duration(intervalSec) * time.Second
}
