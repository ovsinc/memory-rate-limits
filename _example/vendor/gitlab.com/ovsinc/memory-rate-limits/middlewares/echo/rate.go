package echo

import (
	"net/http"

	echo "github.com/labstack/echo/v4"

	rate "gitlab.com/ovsinc/memory-rate-limits/rate"
)

// echo Middleware

func RateLimitMiddleware(logger echo.Logger) echo.MiddlewareFunc {
	conf := rate.RateLimitConfig{
		Skipper:                      rate.DefaultSkipper,
		MemoryLimitBarrierPercentage: rate.DefaultRateLimitConfig.MemoryLimitBarrierPercentage,
		RequestsLimitPerSecond:       rate.DefaultRateLimitConfig.RequestsLimitPerSecond,
		BypassOnError:                rate.DefaultRateLimitConfig.BypassOnError,
		Logger:                       logger,
	}
	return RateLimitWithConfig(conf)
}

func RateLimitWithConfig(config rate.RateLimitConfig) echo.MiddlewareFunc {
	if config.Skipper == nil {
		config.Skipper = rate.DefaultRateLimitConfig.Skipper
	}

	rl := rate.New(config)

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			if config.Skipper(c) {
				return next(c)
			}

			r, _ := rl.Limit()
			switch r {
			case rate.Fails:
				return c.NoContent(http.StatusTooManyRequests)

			default:
				return next(c)
			}
		}
	}
}
