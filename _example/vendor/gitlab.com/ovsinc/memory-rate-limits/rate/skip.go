package rate

type Skipper func(interface{}) bool

func DefaultSkipper(_ interface{}) bool {
	return false
}
