package rate

type Logger interface {
	Errorf(format string, msg ...interface{})
	Warnf(format string, msg ...interface{})
}

//

type noLog struct{}

func (l *noLog) Errorf(format string, msg ...interface{}) {}
func (l *noLog) Warnf(format string, msg ...interface{})  {}
