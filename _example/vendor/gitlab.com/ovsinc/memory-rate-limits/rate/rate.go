package rate

import (
	"gitlab.com/ovsinc/memory-rate-limits/utils/memory"

	"time"

	"go.uber.org/ratelimit"
)

type RateLimitConfig struct {
	Skipper                      Skipper
	MemoryLimitBarrierPercentage float64
	RequestsLimitPerSecond       int
	BypassOnError                bool
	RateOnBypass                 bool
	Logger                       Logger
}

var DefaultRateLimitConfig = RateLimitConfig{
	Skipper:                      DefaultSkipper,
	MemoryLimitBarrierPercentage: 80,
	RequestsLimitPerSecond:       10,
	BypassOnError:                true,
	RateOnBypass:                 true,
	Logger:                       &noLog{},
}

type Limited uint16

const (
	Unlimited Limited = iota
	RateLimited
	FailsBypass
	FailsBypassLimited
	Fails
)

//

type Ilimiter interface {
	Limit() (Limited, time.Time)
}

func New(config RateLimitConfig) Ilimiter {
	return &rateLimiter{
		Limiter: ratelimit.New(config.RequestsLimitPerSecond),
		config:  config,
		memo:    memory.New(),
	}
}

type rateLimiter struct {
	memo memory.IMem
	ratelimit.Limiter
	config RateLimitConfig
}

func (rl *rateLimiter) Limit() (Limited, time.Time) {
	zero := time.Unix(0, 0)

	used, memUserErr := rl.memo.MemUsed()
	total, memTotalerr := rl.memo.MemTotal()

	if memUserErr != nil || memTotalerr != nil {
		rl.config.Logger.
			Errorf("calculate the memory limit fails, bypass on error mode - %t", rl.config.BypassOnError)

		if rl.config.BypassOnError {
			if rl.config.RateOnBypass {
				return FailsBypassLimited, rl.Take()
			}
			return FailsBypass, zero
		}

		return Fails, zero
	}

	percent := float64(used*100) / float64(total)

	if percent < rl.config.MemoryLimitBarrierPercentage {
		return Unlimited, zero
	}

	rl.config.Logger.
		Warnf("the memory limit %v%% has been exceeded. The rate limit mode is enabled.", percent)

	return RateLimited, rl.Take()
}
