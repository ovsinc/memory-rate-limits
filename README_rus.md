# memory-rate-limits

Middleware для ограничение количества запросов после преодоления порога использования ресурсов (в настоящий момент доступно лимитирование по использованию RAM).

Использование RAM рассчитываетс как процент от выделенной квоты.
В случае RAM квотой может быть доступная операционной системе RAM или лимит RAM для CGroup (определяется автоматически).

## Оглавление

0. [Предварительные условия](#Предварительные-условия)
1. [Установка](#Установка)
2. [Тестирование](#Тестирование)
3. [Фичи](#Фичи)
4. [Использование](#Использование)
5. [Список задач](#Список-задач)

____

## Предварительные условия

Используйте любой дистрибутив [Linux](https://distrowatch.com/) и [Go](https://golang.org/).

Может понадобиться [make](http://www.gnu.org/software/make/) для более удобной работы с исходным кодом.
Воспользуйтесь инструкцией по установки пакета `make` в вашем Linux дистрибутиве.

[К оглавлению](#Оглавление)

## Установка

____

### Установка бинарной версии

Скачать бинарные сборки можно со [страницы релизов](https://gitlab.com/ovsinc/memory-rate-limits/-/releases).

### Установка из исходного кода

Для загрузки и установки из исходного кода нужно воспользоваться `go` утилитой:

```text
go get gitlab.com/ovsinc/memory-rate-limits
```

Или клонируйте репозиторий:

```text
git clone https://gitlab.com/ovsinc/memory-rate-limits.git
cd  memory-rate-limits
go test -short -mod=readonly ./...
```

[К оглавлению](#Оглавление)

## Тестирование

Для запуска юнит-тестов перейдите в выполните:

```text
go test -short -mod=readonly gitlab.com/ovsinc/memory-rate-limits/...
```

Или, если было произведено клонирование репозитория, то перейдите в директорию проекта [memory-rate-limits](https://gitlab.com/ovsinc/memory-rate-limits/) и выполните:

```text
make full_tests
```

Для запуска теста скорости выполните:

```text
go test -bench=. -benchmem -mod=readonly gitlab.com/ovsinc/memory-rate-limits/...
```

[К оглавлению](#Оглавление)

## Фичи

- [X] Использованная память (RAM) как триггер для включения rate limit
- [X] Поддерживается определение исполнения в контейнере или нативной системе
- [X] Поддерживаются middleware для:
  - [X] [echo](https://echo.labstack.com/)
  - [X] [gRPC](https://github.com/grpc-ecosystem/go-grpc-middleware)
  - [ ] [Gin](https://github.com/gin-gonic/gin)
  - [ ] [Negroni](https://github.com/didip/tollbooth_negroni)
  - [ ] [Fiber](https://github.com/gofiber/fiber)
  - [ ] [fasthttp](https://github.com/valyala/fasthttp)
  - [ ] [atreugo](https://github.com/savsgio/atreugo)
- [ ] Поддержать использование CPU в качестве триггера для включения rate limit
- [ ] Поддержать исключения на базе:
  - [ ] Списка IP-адресов
  - [ ] Списка URL

[К оглавлению](#Оглавление)

## Использование

### Конфигурация

Конфигурация представлена следующей структурой

```golang
type RateLimitConfig struct {
	Skipper Skipper

	MemoryUsageBarrierPercentage float64

	RequestsLimitPerSecond int

	BypassOnError bool

	RateOnBypass bool

	Logger Logger
}
```

| Параметр | Тип | Значение по умолчанию | Описание |
| -------- | --- | --------------------- |--------- |
| MemoryUsageBarrierPercentage | `float64` | 80 | Предел (в %) использования RAM для включения rate limit |
| RequestsLimitPerSecond | `int` |  10 | Rate limit значение, запросов в секунду |
| BypassOnError | `bool` | true | Включение bypass при ошибке получения информации о ресурсах |
| RateOnBypass | `bool` | true | Использование  rate limit при включении bypass |
| Skipper | `type Skipper func(interface{}) bool` | Not skip | Если функция возвращает `true`, то пропускается обработка |
| Logger | interface с методами `Errorf(format string, msg ...interface{})`,	`Warnf(format string, msg ...interface{})` и `Debugf(format string, msg ...interface{})` | Логгер фреймворка | Интерфейс логгирования |

### Использование библиотеки rate

Работа фреймворка выгладит следующим образом:
- проверяется использования RAM  на превышерие порога с использованием заданного механизма.

Доступны следующие механизмы прверки RAM:
-  gopsutil - более медленные, но всегда работает;
- cGroups (по умолчанию), быстрый, но не работает со старыми ядрами linux.

Для оценки RAM, квота на доступную RAM не может быть меньше 1. В случае получения значения квоты RAM < 1, считается, что произошла ошибка и производися попытка использования режима bypass.

Для вызова конструтора `rateLimiter` нужно использовать конфиг с типом `RateLimitConfig`.

Например:

```golang
package main

import (
  "fmt"
  "gitlab.com/ovsinc/memory-rate-limits/rate"
)

func main() {
  rl := rate.New(rate.DefaultSkipper)

  lim, now := rl.Limit()

  fmt.Prinln(lim, now)
}
```

### Пример использования

Пример для Echo с использование конфигурации по умоланию:

```golang
package main

import (
	"net/http"
	"github.com/labstack/echo/v4"
)

func main() {
    e := echo.New()

    e.Pre(middleware.RemoveTrailingSlash())

    e.Use(middleware.Logger())
    e.Use(mymiddle.RateLimitMiddleware(e.Logger))

	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello, World!")
	})
	e.Logger.Fatal(e.Start(":1323"))
}
```

Более подробно смотрите [примеры репозитория](https://gitlab.com/ovsinc/memory-rate-limits/-/tree/master/_example).

[К оглавлению](#Оглавление)
