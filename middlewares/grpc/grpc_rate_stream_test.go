package grpc_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	rate "gitlab.com/ovsinc/memory-rate-limits"
	"gitlab.com/ovsinc/memory-rate-limits/middlewares/grpc"
	memmock "gitlab.com/ovsinc/memory-rate-limits/utils/memory/mock"
	sysgrpc "google.golang.org/grpc"
)

const errStreamMsgFake = "fake error"

func TestStreamServerInterceptorWithConfigLimited(t *testing.T) {
	t.Parallel()

	limitedCfg := grpc.DefaultConfigStream
	limitedCfg.Rate = rate.New(
		rate.WithConfig(
			&rate.RateLimitConfig{
				MemoryUsageBarrierPercentage: 80,
				ThrottleSecond:               3,
				RetryAfter:                   rate.DefailtRetryAfter,
				LimitReached:                 rate.DefaultLimitedHandler,
				ErrorHandler:                 rate.DefaultErrorHandler,
				Limiter:                      rate.DefaultLimiter,
			},
		),
		rate.WithMemoryChecker(memmock.NewMemLimited()),
	)

	interceptor := grpc.StreamServerInterceptorWithConfig(&limitedCfg)
	handler := func(srv interface{}, stream sysgrpc.ServerStream) error {
		return errors.New(errStreamMsgFake)
	}
	method := "FakeMethod"
	info := &sysgrpc.StreamServerInfo{
		FullMethod: method,
	}

	err := interceptor(nil, nil, info, handler)
	assert.EqualError(t, err, "rpc error: code = ResourceExhausted desc = rejected by memory-rate-limits middleware, please retry later.")
}

func TestStreamServerInterceptorDefault(t *testing.T) {
	t.Parallel()

	interceptor := grpc.StreamServerInterceptor()
	handler := func(srv interface{}, stream sysgrpc.ServerStream) error {
		return errors.New(errStreamMsgFake)
	}
	method := "FakeMethod"
	info := &sysgrpc.StreamServerInfo{
		FullMethod: method,
	}

	err := interceptor(nil, nil, info, handler)
	assert.EqualError(t, err, errStreamMsgFake)
}

func TestStreamServerInterceptorWithConfigFailed(t *testing.T) {
	t.Parallel()

	failCfg := grpc.DefaultConfigStream
	failCfg.Rate = rate.New(
		rate.WithConfig(
			&rate.RateLimitConfig{
				MemoryUsageBarrierPercentage: 80,
				ThrottleSecond:               3,
				RetryAfter:                   rate.DefailtRetryAfter,
				LimitReached:                 rate.DefaultLimitedHandler,
				ErrorHandler:                 rate.DefaultErrorHandler,
				Limiter:                      rate.DefaultLimiter,
			},
		),
		rate.WithMemoryChecker(memmock.NewMemFails()),
	)

	interceptor := grpc.StreamServerInterceptorWithConfig(&failCfg)
	handler := func(srv interface{}, stream sysgrpc.ServerStream) error {
		return errors.New(errStreamMsgFake)
	}
	method := "FakeMethod"
	info := &sysgrpc.StreamServerInfo{
		FullMethod: method,
	}

	err := interceptor(nil, nil, info, handler)
	assert.EqualError(t, err, "rpc error: code = ResourceExhausted desc = FakeMethod is failed by memory-rate-limits middleware, please retry later.")
}
