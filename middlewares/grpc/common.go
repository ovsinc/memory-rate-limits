package grpc

import (
	"context"

	"google.golang.org/grpc/peer"
)

func getIP(ctx context.Context) (ip string) {
	if p, ok := peer.FromContext(ctx); ok {
		ip = p.Addr.String()
	}
	return
}
