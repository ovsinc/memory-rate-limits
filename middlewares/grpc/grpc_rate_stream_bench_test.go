package grpc_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/require"
	sysgrpc "google.golang.org/grpc"

	"gitlab.com/ovsinc/memory-rate-limits/middlewares/grpc"
	middgrpc "gitlab.com/ovsinc/memory-rate-limits/middlewares/grpc"

	rate "gitlab.com/ovsinc/memory-rate-limits"
	memmock "gitlab.com/ovsinc/memory-rate-limits/utils/memory/mock"
)

func BenchmarkRPCStreamWithMiddleware(b *testing.B) {
	cfg := middgrpc.DefaultConfigStream
	cfg.Rate = rate.New(
		rate.WithMemoryChecker(memmock.NewMemUnlimited()),
	)

	interceptor := grpc.StreamServerInterceptorWithConfig(&cfg)
	handler := func(srv interface{}, stream sysgrpc.ServerStream) error {
		return errors.New(errStreamMsgFake)
	}
	method := "FakeMethod"
	info := &sysgrpc.StreamServerInfo{
		FullMethod: method,
	}

	require.EqualError(
		b,
		interceptor(nil, nil, info, handler),
		errStreamMsgFake,
	)

	for i := 0; i < b.N; i++ {
		_ = interceptor(nil, nil, info, handler)
	}
}

func BenchmarkRPCStreamEmptyHendler(b *testing.B) {
	handler := func(srv interface{}, stream sysgrpc.ServerStream) error {
		return errors.New(errStreamMsgFake)
	}

	for i := 0; i < b.N; i++ {
		_ = handler(nil, nil)
	}
}
