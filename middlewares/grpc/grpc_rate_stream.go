package grpc

import (
	"context"
	"errors"

	rate "gitlab.com/ovsinc/memory-rate-limits"
	"gitlab.com/ovsinc/memory-rate-limits/middlewares"
	sysgrpc "google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ConfigStream struct {
	Rate   rate.Limiter
	Config *rate.RateLimitConfig
	Logger middlewares.Logger

	LimitHandler func(
		interface{},
		sysgrpc.ServerStream,
		*sysgrpc.StreamServerInfo,
		*ConfigStream,
	) error

	ErrorHandler func(
		interface{},
		sysgrpc.ServerStream,
		*sysgrpc.StreamServerInfo,
		*ConfigStream,
	) error

	Skip func(
		sysgrpc.StreamHandler,
		*sysgrpc.StreamServerInfo,
		*ConfigStream,
	) bool
}

var DefaultConfigStream = ConfigStream{
	Rate:   rate.New(),
	Config: rate.DefaultRateLimitConfig,
	Logger: nil,

	LimitHandler: func(
		interface{},
		sysgrpc.ServerStream,
		*sysgrpc.StreamServerInfo,
		*ConfigStream,
	) error {
		return status.Errorf(
			codes.ResourceExhausted,
			"rejected by memory-rate-limits middleware, please retry later.",
		)
	},

	ErrorHandler: func(
		_ interface{},
		_ sysgrpc.ServerStream,
		info *sysgrpc.StreamServerInfo,
		_ *ConfigStream,
	) error {
		return status.Errorf(
			codes.ResourceExhausted,
			"%s is failed by memory-rate-limits middleware, please retry later.",
			info.FullMethod,
		)
	},

	Skip: nil,
}

func StreamServerInterceptor() sysgrpc.StreamServerInterceptor {
	return StreamServerInterceptorWithConfig(&DefaultConfigStream)
}

// StreamServerInterceptor returns a new stream server interceptor that performs rate limiting on the request.
func StreamServerInterceptorWithConfig(config *ConfigStream) sysgrpc.StreamServerInterceptor {
	if config.Rate == nil {
		config.Rate = rate.New()
	}

	return func(
		srv interface{},
		stream sysgrpc.ServerStream,
		info *sysgrpc.StreamServerInfo,
		handler sysgrpc.StreamHandler,
	) error {
		if config.Skip != nil && config.Skip(handler, info, config) {
			return handler(srv, stream)
		}

		var ctx context.Context
		if stream != nil {
			ctx = stream.Context()
		}

		memInfo, err := config.Rate.Limit()
		switch {
		case errors.Is(err, rate.ErrRequestLimited):
			if config.Logger != nil {
				config.Logger.Infof(
					"Memory throttle limiter is enabled with parameters: MemoryUsageBarrierPercentage - %f, ThrottleSecond - %d, RetryAfter - %d.",

					config.Config.MemoryUsageBarrierPercentage,
					config.Config.ThrottleSecond,
					config.Config.RetryAfter,
				)
				config.Logger.Warnf(
					"Stream request from %v with method %v is limited",
					getIP(ctx), info.FullMethod,
				)
			}
			return config.LimitHandler(srv, stream, info, config)

		case err != nil:
			if config.Logger != nil {
				config.Logger.Errorf("Memory rate limitter fails with err: %v", err)
			}
			return config.ErrorHandler(srv, stream, info, config)

		default:
			if config.Logger != nil {
				config.Logger.Debugf(
					"Memory used %d of %d (%.2f%)",
					memInfo.Used, memInfo.Total, memInfo.Percent,
				)
			}
		}

		return handler(srv, stream)
	}
}
