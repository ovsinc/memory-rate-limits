package grpc_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	rate "gitlab.com/ovsinc/memory-rate-limits"
	middgrpc "gitlab.com/ovsinc/memory-rate-limits/middlewares/grpc"
	memmock "gitlab.com/ovsinc/memory-rate-limits/utils/memory/mock"
	sysgrpc "google.golang.org/grpc"
)

const errUnaryMsgFake = "fake error"

func TestUnaryServerInterceptorWithConfigLimited(t *testing.T) {
	t.Parallel()

	limitedCfg := middgrpc.DefaultConfigUnary
	limitedCfg.Rate = rate.New(
		rate.WithConfig(
			&rate.RateLimitConfig{
				MemoryUsageBarrierPercentage: 80,
				ThrottleSecond:               3,
				RetryAfter:                   rate.DefailtRetryAfter,
				LimitReached:                 rate.DefaultLimitedHandler,
				ErrorHandler:                 rate.DefaultErrorHandler,
				Limiter:                      rate.DefaultLimiter,
			},
		),
		rate.WithMemoryChecker(memmock.NewMemLimited()),
	)

	interceptor := middgrpc.UnaryServerInterceptorWithConfig(&limitedCfg)
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return nil, errors.New(errUnaryMsgFake)
	}
	info := &sysgrpc.UnaryServerInfo{
		FullMethod: "FakeMethod",
	}
	resp, err := interceptor(nil, nil, info, handler)
	assert.Nil(t, resp)
	assert.EqualError(t, err, "rpc error: code = ResourceExhausted desc = FakeMethod is rejected by memory-rate-limits middleware, please retry later.")
}

func TestUnaryServerInterceptorDefault(t *testing.T) {
	t.Parallel()

	interceptor := middgrpc.UnaryServerInterceptor()
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return nil, errors.New(errUnaryMsgFake)
	}
	info := &sysgrpc.UnaryServerInfo{
		FullMethod: "FakeMethod",
	}
	resp, err := interceptor(nil, nil, info, handler)
	assert.Nil(t, resp)
	assert.EqualError(t, err, errUnaryMsgFake)
}

func TestUnaryServerInterceptorWithConfigFail(t *testing.T) {
	t.Parallel()

	failCfg := middgrpc.DefaultConfigUnary
	failCfg.Rate = rate.New(
		rate.WithConfig(
			&rate.RateLimitConfig{
				MemoryUsageBarrierPercentage: 80,
				ThrottleSecond:               3,
				RetryAfter:                   rate.DefailtRetryAfter,
				LimitReached:                 rate.DefaultLimitedHandler,
				ErrorHandler:                 rate.DefaultErrorHandler,
				Limiter:                      rate.DefaultLimiter,
			},
		),
		rate.WithMemoryChecker(memmock.NewMemFails()),
	)

	interceptor := middgrpc.UnaryServerInterceptorWithConfig(&failCfg)
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return nil, errors.New(errUnaryMsgFake)
	}
	info := &sysgrpc.UnaryServerInfo{
		FullMethod: "FakeMethod",
	}
	resp, err := interceptor(nil, nil, info, handler)
	assert.Nil(t, resp)
	assert.EqualError(t, err, "rpc error: code = ResourceExhausted desc = FakeMethod is failed by memory-rate-limits middleware, please retry later.")
}
