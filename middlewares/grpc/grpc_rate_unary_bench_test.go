package grpc_test

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/require"
	sysgrpc "google.golang.org/grpc"

	middgrpc "gitlab.com/ovsinc/memory-rate-limits/middlewares/grpc"

	rate "gitlab.com/ovsinc/memory-rate-limits"
	memmock "gitlab.com/ovsinc/memory-rate-limits/utils/memory/mock"
)

func BenchmarkRPCUnaryWithMiddleware(b *testing.B) {
	cfg := middgrpc.DefaultConfigUnary
	cfg.Rate = rate.New(
		rate.WithMemoryChecker(memmock.NewMemUnlimited()),
	)

	interceptor := middgrpc.UnaryServerInterceptorWithConfig(&cfg)
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return nil, errors.New(errUnaryMsgFake)
	}
	info := &sysgrpc.UnaryServerInfo{
		FullMethod: "FakeMethod",
	}
	resp, err := interceptor(nil, nil, info, handler)
	require.Nil(b, resp)
	require.EqualError(b, err, errUnaryMsgFake)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = interceptor(nil, nil, info, handler)
	}
}

func BenchmarkRPCUnaryEmptyHandler(b *testing.B) {
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return nil, errors.New(errUnaryMsgFake)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = handler(context.TODO(), nil)
	}
}
