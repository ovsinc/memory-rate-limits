package grpc

import (
	"context"
	"errors"

	rate "gitlab.com/ovsinc/memory-rate-limits"
	"gitlab.com/ovsinc/memory-rate-limits/middlewares"
	sysgrpc "google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ConfigUnary struct {
	Rate   rate.Limiter
	Config *rate.RateLimitConfig
	Logger middlewares.Logger

	LimitHandler func(
		context.Context,
		interface{},
		*sysgrpc.UnaryServerInfo,
		*ConfigUnary,
	) (interface{}, error)

	ErrorHandler func(
		context.Context,
		interface{},
		*sysgrpc.UnaryServerInfo,
		*ConfigUnary,
	) (interface{}, error)

	Skip func(
		context.Context,
		sysgrpc.UnaryHandler,
		*sysgrpc.UnaryServerInfo,
		*ConfigUnary,
	) bool
}

var DefaultConfigUnary = ConfigUnary{
	Rate:   rate.New(),
	Config: rate.DefaultRateLimitConfig,
	Logger: nil,

	LimitHandler: func(
		_ context.Context,
		_ interface{},
		info *sysgrpc.UnaryServerInfo,
		_ *ConfigUnary,
	) (interface{}, error) {
		return nil, status.Errorf(
			codes.ResourceExhausted,
			"%s is rejected by memory-rate-limits middleware, please retry later.",
			info.FullMethod,
		)
	},

	ErrorHandler: func(
		_ context.Context,
		_ interface{},
		info *sysgrpc.UnaryServerInfo,
		_ *ConfigUnary,
	) (interface{}, error) {
		return nil, status.Errorf(
			codes.ResourceExhausted,
			"%s is failed by memory-rate-limits middleware, please retry later.",
			info.FullMethod,
		)
	},

	Skip: func(
		context.Context, sysgrpc.UnaryHandler, *sysgrpc.UnaryServerInfo, *ConfigUnary,
	) bool {
		return false
	},
}

func UnaryServerInterceptor() sysgrpc.UnaryServerInterceptor {
	return UnaryServerInterceptorWithConfig(&DefaultConfigUnary)
}

func UnaryServerInterceptorWithConfig(config *ConfigUnary) sysgrpc.UnaryServerInterceptor {
	if config.Rate == nil {
		config.Rate = rate.New()
	}
	return func(
		ctx context.Context,
		req interface{},
		info *sysgrpc.UnaryServerInfo,
		handler sysgrpc.UnaryHandler,
	) (interface{}, error) {
		if config.Skip != nil && config.Skip(ctx, handler, info, config) {
			return handler(ctx, req)
		}

		memInfo, err := config.Rate.Limit()
		switch {
		case errors.Is(err, rate.ErrRequestLimited):
			if config.Logger != nil {
				config.Logger.Infof(
					"Memory throttle limiter is enabled with parameters: MemoryUsageBarrierPercentage - %f, ThrottleSecond - %d, RetryAfter - %d.",

					config.Config.MemoryUsageBarrierPercentage,
					config.Config.ThrottleSecond,
					config.Config.RetryAfter,
				)

				config.Logger.Warnf(
					"Unary request from %s with method %v is limited",
					getIP(ctx), info.FullMethod,
				)
			}
			return config.LimitHandler(ctx, req, info, config)

		case err != nil:
			if config.Logger != nil {
				config.Logger.Errorf("Memory rate limitter fails with err: %v", err)
			}
			return config.ErrorHandler(ctx, req, info, config)

		default:
			if config.Logger != nil {
				config.Logger.Debugf(
					"Memory used %d of %d (%.2f%)",
					memInfo.Used, memInfo.Total, memInfo.Percent,
				)
			}
		}

		return handler(ctx, req)
	}
}
