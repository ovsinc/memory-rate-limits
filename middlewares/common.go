package middlewares

type Logger interface {
	Errorf(v ...interface{})
	Infof(v ...interface{})
	Warnf(v ...interface{})
	Debugf(v ...interface{})
}

const (
	// HeaderRateLimitRemaining = "X-RateLimit-Remaining"
	HeaderRetryAfter = "Retry-After"
)
