package fiber

import (
	"errors"
	"net/http"
	"strconv"

	sysfiber "github.com/gofiber/fiber/v2"
	rate "gitlab.com/ovsinc/memory-rate-limits"
	"gitlab.com/ovsinc/memory-rate-limits/middlewares"
)

type Config struct {
	Rate         rate.Limiter
	Config       *rate.RateLimitConfig
	Logger       middlewares.Logger
	ErrHandler   func(*sysfiber.Ctx, *Config) error
	LimitHandler func(*sysfiber.Ctx, *Config) error
	Skip         func(*sysfiber.Ctx) bool
}

var DefaultFiberConfig = Config{
	Config:     rate.DefaultRateLimitConfig,
	Skip:       nil,
	Rate:       rate.New(),
	ErrHandler: func(*sysfiber.Ctx, *Config) error { return nil },
	LimitHandler: func(ctx *sysfiber.Ctx, conf *Config) error {
		ctx.Response().Header.Add(
			middlewares.HeaderRetryAfter,
			strconv.Itoa(conf.Config.RetryAfter),
		)
		return ctx.SendStatus(http.StatusTooManyRequests)
	},
}

func RateLimit() sysfiber.Handler {
	return RateLimitWithConfig(&DefaultFiberConfig)
}

// RateLimitWithConfig echo middleware with custom conf
func RateLimitWithConfig(config *Config) sysfiber.Handler {
	if config.Rate == nil {
		config.Rate = rate.New()
	}

	return func(c *sysfiber.Ctx) error {
		if config.Skip != nil && config.Skip(c) {
			return c.Next()
		}

		info, err := config.Rate.Limit()
		switch {
		case errors.Is(err, rate.ErrRequestLimited):
			if config.Logger != nil {
				config.Logger.Infof(
					"Memory throttle limiter is enabled with parameters: MemoryUsageBarrierPercentage - %f, ThrottleSecond - %d, RetryAfter - %d.",

					config.Config.MemoryUsageBarrierPercentage,
					config.Config.ThrottleSecond,
					config.Config.RetryAfter,
				)
				config.Logger.Warnf(
					"Request from %v with path %v is limited", c.IP(), c.Path(),
				)
			}
			return config.LimitHandler(c, config)

		case err != nil:
			if config.Logger != nil {
				config.Logger.Errorf("Memory rate limitter fails with err: %v", err)
			}
			return config.ErrHandler(c, config)

		default:
			if config.Logger != nil {
				config.Logger.Debugf(
					"Memory used %d of %d (%.2f%)",
					info.Used, info.Total, info.Percent,
				)
			}
		}

		return c.Next()
	}
}
