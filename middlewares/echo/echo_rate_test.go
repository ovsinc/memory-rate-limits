package echo_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strconv"
	"testing"

	sysecho "github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	rate "gitlab.com/ovsinc/memory-rate-limits"
	"gitlab.com/ovsinc/memory-rate-limits/middlewares"
	middlecho "gitlab.com/ovsinc/memory-rate-limits/middlewares/echo"
	memmock "gitlab.com/ovsinc/memory-rate-limits/utils/memory/mock"
)

func newApp() (sysecho.Context, sysecho.HandlerFunc, *httptest.ResponseRecorder) {
	e := sysecho.New()

	req := httptest.NewRequest(http.MethodGet, "/", nil)
	rec := httptest.NewRecorder()

	c := e.NewContext(req, rec)

	h := func(c sysecho.Context) error {
		body, err := ioutil.ReadAll(c.Request().Body)
		if err != nil {
			return err
		}
		return c.String(http.StatusOK, string(body))
	}

	return c, h, rec
}

func TestRateLimitDefault(t *testing.T) {
	t.Parallel()

	c, h, rec := newApp()

	midNormal := middlecho.RateLimitMiddleware(c.Echo().Logger)

	assert := assert.New(t)

	if assert.NoError(midNormal(h)(c)) {
		assert.Equal(http.StatusOK, rec.Code)
	}
}

func TestRateLimitWithConfigLimited(t *testing.T) {
	t.Parallel()

	c, h, rec := newApp()

	limCfg := middlecho.DefaultEchoConfig
	limCfg.Rate = rate.New(
		rate.WithConfig(
			&rate.RateLimitConfig{
				MemoryUsageBarrierPercentage: 80,
				ThrottleSecond:               3,
				RetryAfter:                   rate.DefailtRetryAfter,
				LimitReached:                 rate.DefaultLimitedHandler,
				ErrorHandler:                 rate.DefaultErrorHandler,
				Limiter:                      rate.DefaultLimiter,
			},
		),
		rate.WithMemoryChecker(memmock.NewMemLimited()),
	)

	midLimited := middlecho.RateLimitWithConfig(&limCfg)

	assert := assert.New(t)

	if assert.NoError(midLimited(h)(c)) {
		assert.Equal(http.StatusTooManyRequests, rec.Code)
		assert.Equal(
			rec.Header().Get(
				middlewares.HeaderRetryAfter),
			strconv.Itoa(limCfg.Config.RetryAfter),
		)
	}
}

func TestRateLimitWithConfigFails(t *testing.T) {
	t.Parallel()

	c, h, rec := newApp()

	failCfg := middlecho.DefaultEchoConfig
	failCfg.Rate = rate.New(
		rate.WithMemoryChecker(memmock.NewMemFails()),
	)
	failCfg.ErrHandler = func(c sysecho.Context, _ *middlecho.Config) error {
		return c.NoContent(http.StatusInternalServerError)
	}

	errLimited := middlecho.RateLimitWithConfig(&failCfg)

	assert := assert.New(t)

	if assert.NoError(errLimited(h)(c)) {
		assert.Equal(http.StatusInternalServerError, rec.Code)
	}
}
