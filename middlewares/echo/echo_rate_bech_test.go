package echo_test

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"

	rate "gitlab.com/ovsinc/memory-rate-limits"
	middlecho "gitlab.com/ovsinc/memory-rate-limits/middlewares/echo"
	memmock "gitlab.com/ovsinc/memory-rate-limits/utils/memory/mock"
)

func BenchmarkEchoEmpty(b *testing.B) {
	c, h, rec := newApp()

	require.Nil(b, h(c))
	require.Equal(b, http.StatusOK, rec.Code)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = h(c)
	}
}

func BenchmarkEchoWithMiddleware(b *testing.B) {
	cfg := middlecho.DefaultEchoConfig
	cfg.Rate = rate.New(
		rate.WithMemoryChecker(memmock.NewMemUnlimited()),
	)

	c, h, rec := newApp()

	require.Nil(b, middlecho.RateLimitWithConfig(&cfg)(h)(c))
	require.Equal(b, http.StatusOK, rec.Code)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = middlecho.RateLimitWithConfig(&cfg)(h)(c)
	}
}
