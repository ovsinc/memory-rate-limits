package echo

import (
	"errors"
	"net/http"
	"strconv"

	sysecho "github.com/labstack/echo/v4"
	middleware "github.com/labstack/echo/v4/middleware"
	rate "gitlab.com/ovsinc/memory-rate-limits"
	"gitlab.com/ovsinc/memory-rate-limits/middlewares"
)

// echo Middleware

type Config struct {
	Rate         rate.Limiter
	Config       *rate.RateLimitConfig
	Logger       sysecho.Logger
	ErrHandler   func(c sysecho.Context, cfg *Config) error
	LimitHandler func(c sysecho.Context, cfg *Config) error
	Skip         middleware.Skipper
}

var DefaultEchoConfig = Config{
	Config:     rate.DefaultRateLimitConfig,
	Skip:       middleware.DefaultSkipper,
	Rate:       rate.New(),
	ErrHandler: func(c sysecho.Context, cfg *Config) error { return nil },
	LimitHandler: func(c sysecho.Context, cfg *Config) error {
		c.Response().Header().Add(
			middlewares.HeaderRetryAfter,
			strconv.Itoa(cfg.Config.RetryAfter),
		)
		return c.NoContent(http.StatusTooManyRequests)
	},
}

// RateLimitMiddleware echo middleware with default conf
func RateLimitMiddleware(logger sysecho.Logger) sysecho.MiddlewareFunc {
	conf := DefaultEchoConfig
	conf.Logger = logger
	return RateLimitWithConfig(&conf)
}

// RateLimitWithConfig echo middleware with custom conf
func RateLimitWithConfig(config *Config) sysecho.MiddlewareFunc {
	if config.Rate == nil {
		config.Rate = rate.New()
	}

	return func(next sysecho.HandlerFunc) sysecho.HandlerFunc {
		return func(c sysecho.Context) error {
			if config.Skip != nil && config.Skip(c) {
				return next(c)
			}

			info, err := config.Rate.Limit()
			switch {
			case errors.Is(err, rate.ErrRequestLimited):
				if config.Logger != nil {
					config.Logger.Infof(
						"Memory throttle limiter is enabled with parameters: MemoryUsageBarrierPercentage - %f, ThrottleSecond - %d, RetryAfter - %d.",

						config.Config.MemoryUsageBarrierPercentage,
						config.Config.ThrottleSecond,
						config.Config.RetryAfter,
					)
					config.Logger.Warnf(
						"Request from %v with path %v is limited", c.RealIP(), c.Path(),
					)
				}
				return config.LimitHandler(c, config)

			case err != nil:
				if config.Logger != nil {
					config.Logger.Errorf("Memory rate limitter fails with err: %v", err)
				}
				return config.ErrHandler(c, config)

			default:
				if config.Logger != nil {
					config.Logger.Debugf(
						"Memory used %d of %d (%.2f%)",
						info.Used, info.Total, info.Percent,
					)
				}
			}

			return next(c)
		}
	}
}
