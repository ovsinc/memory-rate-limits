package rate

import "gitlab.com/ovsinc/memory-rate-limits/utils/memory"

type Option func(*rateLimiter)

func WithMemoryChecker(memo memory.IMem) Option {
	return func(l *rateLimiter) {
		if l == nil {
			return
		}
		l.memo = memo
	}
}

func WithConfig(conf *RateLimitConfig) Option {
	return func(l *rateLimiter) {
		if l == nil {
			return
		}
		l.config = conf
	}
}
