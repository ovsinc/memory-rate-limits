package rate

import (
	"testing"

	"gitlab.com/ovsinc/memory-rate-limits/utils/memory"
	memmock "gitlab.com/ovsinc/memory-rate-limits/utils/memory/mock"
)

func BenchmarkLimit_UnlimMoc(b *testing.B) {
	rl := &rateLimiter{
		config: confUnlimited,
		memo:   memmock.NewMemUnlimited(),
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = rl.Limit()
	}
}

func BenchmarkLimit_UnlimMemNorm(b *testing.B) {
	rl := &rateLimiter{
		config: confUnlimited,
		memo:   &memory.MemNormal{},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = rl.Limit()
	}
}

func BenchmarkLimit_UnlimMemCG(b *testing.B) {
	rl := &rateLimiter{
		config: confBypass,
		memo:   &memory.MemCGroup{},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = rl.Limit()
	}
}
