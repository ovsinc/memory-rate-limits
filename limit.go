package rate

import "go.uber.org/ratelimit"

func NewLimited(rate int) ratelimit.Limiter {
	return ratelimit.New(rate)
}

func NewUnlimited() ratelimit.Limiter {
	return ratelimit.NewUnlimited()
}
