package utils

import (
	"io/ioutil"
	"math"
	"os"
	"strconv"
	"testing"

	"github.com/shirou/gopsutil/v3/mem"
)

func Test_parseUint(t *testing.T) {
	type args struct {
		s []byte
	}
	tests := []struct {
		name    string
		args    args
		want    uint64
		wantErr bool
	}{
		{
			name: "zero",
			args: args{
				s: []byte("0"),
			},
			want:    0,
			wantErr: false,
		},
		{
			name: "123",
			args: args{
				s: []byte("123"),
			},
			want:    123,
			wantErr: false,
		},
		{
			name: "-1",
			args: args{
				s: []byte("-1"),
			},
			want:    0,
			wantErr: false,
		},
		{
			name: "maxUint64",
			args: args{
				s: []byte("18446744073709551615"),
			},
			want:    math.MaxUint64,
			wantErr: false,
		},
		{
			name: "maxInt64",
			args: args{
				s: []byte("9223372036854775807"),
			},
			want:    math.MaxInt64,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			got, err := parseUint(tt.args.s)
			if (err != nil) != tt.wantErr {
				t.Errorf("parseUint() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("parseUint() = %v, want %v", got, tt.want)
			}
		})
	}
}

func prepareTestFile() string {
	f, err := ioutil.TempFile("/tmp", "*")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	memInfo, err := mem.VirtualMemory()
	if err != nil {
		panic(err)
	}

	_, _ = f.WriteString(strconv.Itoa(int(memInfo.Used)))

	return f.Name()
}

func BenchmarkParse_ReadUint(b *testing.B) {
	usageFile := prepareTestFile()
	defer os.Remove(usageFile)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := ReadUint(usageFile)
		if err != nil {
			b.Errorf("ReadUint() error = %v", err)
		}
	}
}

func BenchmarkParse_NewParse_ReadUint(b *testing.B) {
	usageFile := prepareTestFile()
	defer os.Remove(usageFile)

	p, err := NewParse(usageFile)
	if err != nil {
		b.Errorf("NewParse() error = %v", err)
	}
	defer p.Close()

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err = p.ReadUint()
		if err != nil {
			b.Errorf("ReadUint() error = %v", err)
		}
	}
}

func BenchmarkReadIoUtil(b *testing.B) {
	usageFile := prepareTestFile()
	defer os.Remove(usageFile)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := ioutil.ReadFile(usageFile)
		if err != nil {
			b.Fatalf("cant read")
		}
	}
}

func BenchmarkReadOs(b *testing.B) {
	usageFile := prepareTestFile()
	defer os.Remove(usageFile)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		f, err := os.Open(usageFile)
		if err != nil {
			b.Fatalf("cant open")
		}

		data := make([]byte, 9)

		n, err := f.Read(data)
		if err != nil {
			b.Errorf("cant read")
		}

		if n != 9 {
			b.Errorf("bad count got %v need %v", n, 9)
		}

		f.Close()
	}
}

func BenchmarkReadOsNotClose(b *testing.B) {
	usageFile := prepareTestFile()
	defer os.Remove(usageFile)

	f, err := os.Open(usageFile)
	if err != nil {
		b.Fatalf("cant open")
	}

	defer f.Close()

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data := make([]byte, 9)

		_, _ = f.Seek(0, 0)

		n, err := f.Read(data)
		if err != nil {
			b.Errorf("cant read")
		}

		if n != 9 {
			b.Errorf("bad count got %v need %v", n, 9)
		}
	}
}
