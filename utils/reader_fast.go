package utils

import (
	"bytes"
	"errors"
	"io"
	"os"
)

// ErrNotOpen read from not opened file
var ErrNotOpen = errors.New("file not open")

func NewParse(path string) (*Parse, error) {
	p := &Parse{}
	return p, p.Open(path)
}

// Parse parser for known files
type Parse struct {
	f *os.File
}

// Open open file for read only
func (p *Parse) Open(path string) (err error) {
	p.f.Close()
	p.f, err = os.Open(path)
	return err
}

// Close close opened file
// do not forget close it when file is unnided
func (p *Parse) Close() {
	p.f.Close()
}

// ReadUint read and parse uit64
func (p *Parse) ReadUint() (uint64, error) {
	if p.f == nil {
		return 0, ErrNotOpen
	}

	data := make([]byte, readBytesLen)

	_, _ = p.f.Seek(0, 0)

	_, err := p.f.Read(data)
	if !(err != nil || errors.Is(err, io.EOF)) {
		return 0, err
	}

	return parseUint(bytes.TrimRight(data, "\n "))
}
