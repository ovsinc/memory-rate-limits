package utils

import (
	"os"
)

// ReadUint read and parse uint64 from file, read only 9 bytes
func ReadUint(path string) (uint64, error) {
	f, err := os.Open(path)
	if err != nil {
		return 0, err
	}
	defer f.Close()

	return ReadUintFromF(f)
}
