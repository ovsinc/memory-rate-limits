package utils

import (
	"bytes"
	"errors"
	"io"
	"strconv"
)

const readBytesLen = 10 // int64 size + '0x0a' + '0x00'

// var validInt = regexp.MustCompile("[-0-9]+")

// refer to https://github.com/containerd/cgroups/blob/318312a373405e5e91134d8063d04d59768a1bff/utils.go#L251
func parseUint(b []byte) (uint64, error) {
	s := string(b)
	v, err := strconv.ParseUint(s, 10, 64)
	if err != nil {
		intValue, intErr := strconv.ParseInt(s, 10, 64)
		// 1. Handle negative values greater than MinInt64 (and)
		// 2. Handle negative values lesser than MinInt64
		if intErr == nil && intValue < 0 {
			return 0, nil
		} else if intErr != nil &&
			// intErr.(*strconv.NumError).Err == strconv.ErrRange &&
			errors.Is(intErr, strconv.ErrRange) &&
			intValue < 0 {
			return 0, nil
		}
		return 0, err
	}
	return v, nil
}

var ErrUintParse = errors.New("parse uint64 fails")

// func parseUint(b []byte) (uint64, error) {
// 	// ret, n := binary.Varint(b)
// 	// switch {
// 	// case n == 0, n < 0:
// 	// 	return 0, ErrUintParse
// 	// case ret < 0:
// 	// 	return 0, nil
// 	// }

// 	buf := [8]byte{}

// 	for i:=len(b)-1;

// 	switch {
// 	case len(b) == 0:
// 		return 0, ErrUintParse
// 	case b[0] == '-':
// 		return 0, nil
// 	}

// 	// var ret int64
// 	// buf := bytes.NewReader(b[:8])
// 	// err := binary.Read(buf, binary.LittleEndian, &ret)
// 	// if err != nil {
// 	// 	return 0, err
// 	// }

// 	return uvarint(8), nil
// }

func ReadUintFromF(f io.Reader) (uint64, error) {
	v := make([]byte, readBytesLen)

	if _, err := f.Read(v); !(err == nil || errors.Is(err, io.EOF)) {
		return 0, err
	}

	return parseUint(bytes.TrimRight(v, "\n "))
}
