package memory

import (
	"github.com/shirou/gopsutil/v3/mem"
	"gitlab.com/ovsinc/memory-rate-limits/utils"
)

// New ...
func New() IMem {
	if utils.InContainer() {
		return &MemCGroup{}
	}
	return &MemNormal{}
}

type MemNormal struct{}

// MemTotal returns the total amount of RAM on this system in non-container environment.
func (mn *MemNormal) MemTotal() (uint64, error) {
	v, err := mem.VirtualMemory()
	return v.Total, err
}

// MemUsed returns the total used amount of RAM on this system in non-container environment.
func (mn *MemNormal) MemUsed() (uint64, error) {
	v, err := mem.VirtualMemory()
	return v.Used, err
}
