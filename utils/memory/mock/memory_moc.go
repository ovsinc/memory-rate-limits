package mock

import (
	"errors"
)

func NewMemUnlimited() *memUnlim {
	return &memUnlim{}
}

type memUnlim struct{}

const (
	MemUnlimTotal = 100
	MemUnlimUsed  = 10
)

func (u *memUnlim) MemTotal() (uint64, error) {
	return MemUnlimTotal, nil
}

func (u *memUnlim) MemUsed() (uint64, error) {
	return MemUnlimUsed, nil
}

//

func NewMemLimited() *memLim {
	return &memLim{}
}

type memLim struct{}

const (
	MemLimTotal = 100
	MemLimUsed  = 90
)

func (u *memLim) MemTotal() (uint64, error) {
	return MemLimTotal, nil
}

func (u *memLim) MemUsed() (uint64, error) {
	return MemLimUsed, nil
}

//

func NewMemFails() *memFails {
	return &memFails{}
}

const (
	MemFailsTotal = 0
	MemFailsUsed  = 0
)

type memFails struct{}

var (
	ErrMemFailsTotal = errors.New("bad MemFailsTotal")
	ErrMemFailsUsed  = errors.New("bad MemFailsUsed")
)

func (u *memFails) MemTotal() (uint64, error) {
	return MemFailsTotal, ErrMemFailsTotal
}

func (u *memFails) MemUsed() (uint64, error) {
	return MemFailsUsed, ErrMemFailsUsed
}
