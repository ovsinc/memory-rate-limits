// Based on PingCAP code
// see https://github.com/pingcap/tidb/blob/084e7190b8124f4b53ce83064547042a77ce3ae4/util/memory/meminfo.go

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// See the License for the specific language governing permissions and
// limitations under the License.

package memory

const (
	cGroupMemLimitPath = "/sys/fs/cgroup/memory/memory.limit_in_bytes"
	cGroupMemUsagePath = "/sys/fs/cgroup/memory/memory.usage_in_bytes"
)

// IMem memory amound interface
type IMem interface {
	// MemTotal returns the total amount of RAM on this system
	MemTotal() (uint64, error)

	// MemUsed returns the total used amount of RAM on this system
	MemUsed() (uint64, error)
}
