package memory

import (
	"sync/atomic"

	"gitlab.com/ovsinc/memory-rate-limits/utils"
)

type MemCGroup struct {
	memLimitCache  uint64
	memLimitCached uint32
}

// MemTotal returns the total amount of RAM on this system in container environment.
func (mcg *MemCGroup) MemTotal() (uint64, error) {
	cacheFlag := atomic.LoadUint32(&mcg.memLimitCached)
	if cacheFlag > 0 {
		return mcg.memLimitCache, nil
	}

	var err error

	mcg.memLimitCache, err = utils.ReadUint(cGroupMemLimitPath)
	if err != nil {
		return 0, err
	}

	atomic.StoreUint32(&mcg.memLimitCached, 1)

	return mcg.memLimitCache, nil
}

// MemUsed returns the total used amount of RAM on this system in container environment.
func (mcg *MemCGroup) MemUsed() (uint64, error) {
	return utils.ReadUint(cGroupMemUsagePath)
}
