package utils

import (
	"io/ioutil"
	"strings"
)

const selfCGroupPath = "/proc/self/cgroup"

// InContainer check is in conteiner
func InContainer() bool {
	v, err := ioutil.ReadFile(selfCGroupPath)
	if err != nil {
		return false
	}
	if strings.Contains(string(v), "docker") ||
		strings.Contains(string(v), "kubepods") ||
		strings.Contains(string(v), "containerd") {
		return true
	}
	return false
}
