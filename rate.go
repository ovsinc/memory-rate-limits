package rate

import (
	"errors"

	"gitlab.com/ovsinc/memory-rate-limits/utils/memory"
)

var (
	ErrTotalMemoryZero = errors.New("total memory is zero")
	ErrRequestLimited  = errors.New("request is limited")
)

type MemInfo struct {
	Used    uint64
	Total   uint64
	Percent float64
}

// Limiter is limiter interface
type Limiter interface {
	Limit() (*MemInfo, error)
}

var _ Limiter = (*rateLimiter)(nil)

func percentage(cur, total uint64) float64 {
	if total < 1 {
		return 0
	}
	return float64(cur*100) / float64(total)
}

// New Limiter constructor
func New(ops ...Option) *rateLimiter {
	rl := &rateLimiter{}

	for _, op := range ops {
		op(rl)
	}

	if rl.config == nil {
		rl.config = DefaultRateLimitConfig
	}

	if rl.memo == nil {
		rl.memo = memory.New()
	}

	return rl
}

type rateLimiter struct {
	memo   memory.IMem
	config *RateLimitConfig
}

func (rl *rateLimiter) Limit() (*MemInfo, error) {
	if rl.config.MemoryUsageBarrierPercentage < 1 {
		return &MemInfo{}, nil
	}

	totalMem, err := rl.memo.MemTotal()
	if err != nil {
		if rl.config.ErrorHandler != nil {
			rl.config.ErrorHandler(rl.config, err)
		}
		return nil, err
	}

	usedMem, err := rl.memo.MemUsed()
	switch {
	case err != nil:
		if rl.config.ErrorHandler != nil {
			rl.config.ErrorHandler(rl.config, err)
		}
		return nil, err

	case totalMem < 1:
		if rl.config.ErrorHandler != nil {
			rl.config.ErrorHandler(rl.config, ErrTotalMemoryZero)
		}
		return nil, err
	}

	percentMem := percentage(usedMem, totalMem)

	if percentMem > rl.config.MemoryUsageBarrierPercentage {
		rl.config.LimitReached(rl.config)
		return &MemInfo{
			Total:   totalMem,
			Used:    usedMem,
			Percent: percentMem,
		}, ErrRequestLimited
	}

	return &MemInfo{
		Total:   totalMem,
		Used:    usedMem,
		Percent: percentMem,
	}, nil
}
