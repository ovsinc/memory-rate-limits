package rate

type (
	Handler       func(*RateLimitConfig)
	ErrordHandler func(*RateLimitConfig, error)
)

var (
	DefaultLimitedHandler = func(cfg *RateLimitConfig) {
		if cfg != nil && cfg.Limiter != nil {
			cfg.Limiter.Take()
		}
	}

	DefaultBypassHandler = func(cfg *RateLimitConfig) {}

	DefaultErrorHandler = func(cfg *RateLimitConfig, err error) {
		if cfg != nil && cfg.Limiter != nil {
			cfg.Limiter.Take()
		}
	}
	DefaultBypassErrorHandler = func(cfg *RateLimitConfig) {}
)
