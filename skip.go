package rate

// Skipper skip function
type Skipper func(interface{}) bool

//

// NotSkip defalt dummy skip function
func NotSkip(_ interface{}) bool {
	return false
}
