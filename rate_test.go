package rate

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/ovsinc/memory-rate-limits/utils/memory"
	memmock "gitlab.com/ovsinc/memory-rate-limits/utils/memory/mock"
)

var (
	confLimited = &RateLimitConfig{
		MemoryUsageBarrierPercentage: 80,
		ThrottleSecond:               10,
		RetryAfter:                   3,
		LimitReached: func(cfg *RateLimitConfig) {
			cfg.Limiter.Take()
		},
		Limiter: NewLimited(10),
	}

	confUnlimited = &RateLimitConfig{
		MemoryUsageBarrierPercentage: 80,
		ThrottleSecond:               10,
		RetryAfter:                   3,
		LimitReached: func(cfg *RateLimitConfig) {
			cfg.Limiter.Take()
		},
		Limiter: NewUnlimited(),
	}

	confBypass = &RateLimitConfig{
		MemoryUsageBarrierPercentage: 80,
		ThrottleSecond:               3,
		RetryAfter:                   3,
		LimitReached:                 func(cfg *RateLimitConfig) {},
		Limiter:                      NewLimited(10),
	}
)

func inTimeSpan(start, end, check time.Time) bool {
	return check.After(start) && check.Before(end)
}

func Test_rateLimiter_Limit(t *testing.T) {
	t.Parallel()

	type fields struct {
		memo   memory.IMem
		config *RateLimitConfig
	}
	tests := []struct {
		name    string
		fields  fields
		want    *MemInfo
		wantErr error
		durMax  time.Duration
		durMin  time.Duration
	}{
		{
			name: "unlimit",
			fields: fields{
				config: confLimited,
				memo:   memmock.NewMemUnlimited(),
			},
			durMax: 10 * time.Millisecond,
			durMin: 1 * time.Millisecond,
			want: &MemInfo{
				Total:   memmock.MemUnlimTotal,
				Used:    memmock.MemUnlimUsed,
				Percent: memmock.MemUnlimUsed,
			},
			wantErr: nil,
		},
		{
			name: "limit",
			fields: fields{
				config: confLimited,
				memo:   memmock.NewMemLimited(),
			},
			durMax: 10 * time.Second,
			durMin: 3 * time.Second,
			want: &MemInfo{
				Total:   memmock.MemLimTotal,
				Used:    memmock.MemLimUsed,
				Percent: memmock.MemLimUsed,
			},
			wantErr: ErrRequestLimited,
		},
		{
			name: "bypass with limit",
			fields: fields{
				config: confBypass,
				memo:   memmock.NewMemLimited(),
			},
			durMax: 10 * time.Millisecond,
			durMin: 1 * time.Millisecond,
			want: &MemInfo{
				Total:   memmock.MemLimTotal,
				Used:    memmock.MemLimUsed,
				Percent: memmock.MemLimUsed,
			},
			wantErr: ErrRequestLimited,
		},
		{
			name: "fails",
			fields: fields{
				config: confBypass,
				memo:   memmock.NewMemFails(),
			},
			durMax: 10 * time.Millisecond,
			durMin: 1 * time.Millisecond,
			want: &MemInfo{
				Total:   memmock.MemFailsTotal,
				Used:    memmock.MemFailsUsed,
				Percent: 0,
			},
			wantErr: memmock.ErrMemFailsTotal,
		},
	}
	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			rl := &rateLimiter{
				memo:   tt.fields.memo,
				config: tt.fields.config,
			}

			start := time.Now()
			_, _ = rl.Limit()
			end := time.Now()

			if !inTimeSpan(start, end.Add(tt.durMax), end) {
				t.Errorf("limit func duration is too slow - %v needs %v", start.Sub(end).Milliseconds(), tt.durMax)
				return
			}

			if end.After(start.Add(tt.durMin)) {
				t.Errorf("limit func duration is too fast - %v need %v", start.Sub(end).Milliseconds(), tt.durMin)
			}
		})
	}
}

func TestNew(t *testing.T) {
	t.Parallel()

	un := memmock.NewMemUnlimited()

	type args struct {
		ops []Option
	}
	tests := []struct {
		name string
		args args
		want *rateLimiter
	}{
		{
			name: "empty",
			args: args{
				ops: nil,
			},
			want: New(),
		},
		{
			name: "unlim",
			args: args{
				ops: []Option{
					WithMemoryChecker(un),
				},
			},
			want: New(WithMemoryChecker(un)),
		},
	}
	for _, tt := range tests {
		tt := tt

		t.Run(tt.name, func(t *testing.T) {
			if got := New(tt.args.ops...); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}
