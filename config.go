package rate

import "go.uber.org/ratelimit"

const (
	DefaultThrottleSecond               = 10
	DefaultMemoryUsageBarrierPercentage = 80
	DefailtRetryAfter                   = 3
)

var DefaultLimiter = ratelimit.New(DefaultThrottleSecond)

// RateLimitConfig rate limit conf
type RateLimitConfig struct {
	// MemoryUsageBarrierPercentage the memory usage barrier after which the throttle is enabled
	MemoryUsageBarrierPercentage float64

	// ThrottleSecond requests rate limit throttling
	ThrottleSecond int

	// RetryAfter the value of the Retry-After header
	// indicates how many seconds the client should wait until the next attempt
	RetryAfter int

	// LimitReached is called when a request hits the limit
	LimitReached Handler

	// Limiter throttle limiter to the given RPS
	Limiter ratelimit.Limiter

	// ErrorHandler is called when a some error happends (memory calculation fails, etc)
	ErrorHandler ErrordHandler
}

// DefaultRateLimitConfig default config
var DefaultRateLimitConfig = &RateLimitConfig{
	MemoryUsageBarrierPercentage: DefaultMemoryUsageBarrierPercentage,
	ThrottleSecond:               DefaultThrottleSecond,
	RetryAfter:                   DefailtRetryAfter,
	Limiter:                      DefaultLimiter,
	LimitReached:                 DefaultLimitedHandler,
	ErrorHandler:                 DefaultErrorHandler,
}
